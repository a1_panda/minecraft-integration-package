# Minecraft整合包

#### 介绍
这是一个基于Fabric整合的辅助模组包

#### 安装教程

1.  [下载整合包](http://gitee.com/a1_panda/minecraft-integration-package/releases/)
2.  导入HMCL启动器
3.  添加整合包
5.  启动游戏

#### HMCL启动器下载

* [下载启动器](https://gitee.com/a1_panda/minecraft-integration-package/raw/master/HMCL-3.3.172.exe) 
